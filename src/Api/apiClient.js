import axios from "axios";

const apiClient = axios.create({
    baseURL: `https://api.sheety.co/af35b536915ec576818d468cf2a6505c/reactjsTest`
})


//for bearer token auth 
apiClient.interceptors.request.use((request) => {
    const accessToken = `Ex9yLyRU7wvyxfblpq5HAhfQqUP1vIyo`
    request.headers.Authorization = `Bearer ${accessToken}`
    return request;
})


//to handle if there    ARE ANY ERROS IN INTYERCEPTOR 
apiClient.interceptors.response.use(
    (response) => response,
    (error) => {
      console.error("Axios Request Error:", error);
      return Promise.reject(error);
    }
  );


  
export default apiClient