import apiClient from "./apiClient";


export const productReference = ()=>{
  return apiClient.get("products")
}
export const productColor = ()=>{
  return apiClient.get("colors")
}
export const productMaterial = ()=>{
  return apiClient.get("material")
}
export const productFeatured = ()=>{
  return apiClient.get("featured")
}





// const response1 = await axios.get('https://api.sheety.co/af35b536915ec576818d468cf2a6505c/reactjsTest/products', config);
// const data1 = response1.data.products;
// setProductReference(data1);
// console.log(data1)

// const response2 = await axios.get('https://api.sheety.co/af35b536915ec576818d468cf2a6505c/reactjsTest/colors', config);
// const data2 = response2.data.colors;
// setProductColors(data2);
// console.log(data2)


// const response3 = await axios.get('https://api.sheety.co/af35b536915ec576818d468cf2a6505c/reactjsTest/material', config);
// const data3 = response3.data.material;
// setProductMaterials(data3);
// console.log(data3)


// const response4 = await axios.get('https://api.sheety.co/af35b536915ec576818d468cf2a6505c/reactjsTest/featured', config);
// const data4 = response4.data.featured;
// setProductFeatured(data4);
// console.log(data4)

//       } catch (error) {