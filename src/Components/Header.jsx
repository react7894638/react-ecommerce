import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route, Link, NavLink } from "react-router-dom";
import "../Styles/Header.scss";
import { useProductContext } from "../Context/ProductContext";

const Header = () => {

  const {cartCounter} = useProductContext()


  return (
    <div className="header">
      <div className="header-link">
        <NavLink  to="/All-products">All products</NavLink>
        <NavLink to="/Featured-Products">Featured Products</NavLink>
      </div>
      <div>
        <span>
          <i className="fa-solid fa-cart-shopping"></i>
        </span>
        <span>{cartCounter}</span>
      </div>
    </div>
  );
};

export default Header;
