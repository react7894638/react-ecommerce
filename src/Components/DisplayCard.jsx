import React, { Fragment } from 'react';
import { useProductContext } from '../Context/ProductContext';

const DisplayCard = ({ product, color, material }) => {
  // const { allProducts, color, material, featured } = useProductContext();
  const {increaseCartCounter} = useProductContext()



  return (
    <div className={`display-card`}>
      <Fragment>
        <div className={`img-container`}>
          <a onClick={increaseCartCounter} className="cart-text">
            Add to Cart
          </a>
          <img
            src={product.image}
            alt={product.name}
          />
        </div>
        <h3>{product.name}</h3>
        <span className="display-text-1">
          {color.find(c => c.id === product.colorId)?.name || 'Color Not Found'}
        </span>
        <span className="display-text-2">
          {material.find(m => m.id === product.materialId)?.name || 'Material Not Found'}
        </span>
        <p>{`INR ${product.price}`}</p>
      </Fragment>
    </div>
  );
};

export default DisplayCard;




// { increaseCart, productReference, productColors, productMaterials, productFeatured }
// const [imageLoaded, setImageLoaded] = useState(false);

// console.log(productColors.name)
// const handleImageLoad = () => {
//   setImageLoaded(true);
// };

// ${imageLoaded ? "image-loaded" : ""}

// ${imageLoaded ? "image-loaded" : ""}

// onClick={increaseCart}

//changes routes new file where i have all my routes as per new react router version in the root file, the component needs to be in a page useeffect in all products, need to get all api data when needed in the component, loader, on loader if api status is pending or not thren only use loader us estring more