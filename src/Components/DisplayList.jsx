import React from 'react';
import { useProductContext } from '../Context/ProductContext';

//loader, if both fields are empty- no data, css hover 


const DisplayList = ({
  selectedColor,
  setSelectedColor,
  selectedMaterial,
  setSelectedMaterial,
  filteredProducts,
}) => {
  const { allProducts, color, material, featured } = useProductContext();

  console.log(selectedMaterial);
  console.log(selectedColor);
  console.log(allProducts);

  const handleColorClick = (color) => {
    setSelectedColor(color);
  };

  const handleMaterialClick = (material) => {
    setSelectedMaterial(material);
  };

  const handleAllClick = () => {
    setSelectedColor('All');
    setSelectedMaterial('All');
  };

  return (
    <div className="display-list">
      <h3>Materials</h3>
      <button onClick={() => handleAllClick()}
        className={selectedMaterial === "All" ? 'selected' : ''}

      >All</button>

      {material.map((mat) => (
        <button
          key={mat.id}
          onClick={() => handleMaterialClick(mat.name)}
          className={selectedMaterial === mat.name ? 'selected' : ''}
        >
          {mat.name}
        </button>
      ))}

      <h3>Colors</h3>
      <button onClick={() => handleAllClick()}
        className={selectedMaterial === "All" ? 'selected' : ''}
      >All</button>
      {color.map((col) => (
        <button
          key={col.id}
          onClick={() => handleColorClick(col.name)}
          className={selectedColor === col.name ? 'selected' : ''}
        >
          {col.name}
        </button>
      ))}

    </div>
  );
};

export default DisplayList;
