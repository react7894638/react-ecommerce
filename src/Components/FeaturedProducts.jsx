import React, { useState } from 'react';
import { useProductContext } from '../Context/ProductContext';
import DisplayCard from './DisplayCard';
import DisplayList from './DisplayList';
import Header from './Header';

const FeaturedProducts = () => {
  const { allProducts, color, material, featured } = useProductContext();
  const [selectedColor, setSelectedColor] = useState('All');
  const [selectedMaterial, setSelectedMaterial] = useState('All');

  // Filter featured products based on selected color and material
  const featuredFilteredProducts = allProducts.filter((product) => {
    const productColor =
      color.find((c) => c.id === product.colorId)?.name || '';
    const productMaterial =
      material.find((m) => m.id === product.materialId)?.name || '';

    const colorMatch =
      selectedColor === 'All' || productColor === selectedColor;

    const materialMatch =
      selectedMaterial === 'All' || productMaterial === selectedMaterial;
;
    return colorMatch && materialMatch && featured.some(f => f.productId === product.id)
  });

  return (
    <div className="container">
      <h2>MYCOOLSHOP.COM</h2>
      <Header />
      <div className="display">
        <DisplayList
          selectedColor={selectedColor}
          setSelectedColor={setSelectedColor}
          selectedMaterial={selectedMaterial}
          setSelectedMaterial={setSelectedMaterial}
        />
        <div className="card-container">
          {featuredFilteredProducts.length === 0 ? (
            <p className='no-data'>No featured products match the selected filters.</p>
          ) : (
            featuredFilteredProducts.map((product, index) => (
              <DisplayCard
                key={index}
                product={product}
                color={color}
                material={material}
              />
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default FeaturedProducts;
