import React, { Fragment, useState, useEffect } from 'react';
import '../App.scss';
import DisplayCard from './DisplayCard';
import Header from './Header';
import { useProductContext } from '../Context/ProductContext';
import DisplayList from './DisplayList';

const AllProducts = () => {
  const [selectedColor, setSelectedColor] = useState('All');
  const [selectedMaterial, setSelectedMaterial] = useState('All');
  const { allProducts, color, material, featured, increaseCartCounter } = useProductContext();


  // Filter products based on selected color and material
  const filteredProducts = allProducts.filter((product) => {
    const productColor = color.find((c) => c.id === product.colorId)?.name || '';
    const productMaterial = material.find((m) => m.id === product.materialId)?.name || '';

    const colorMatch = selectedColor === 'All' || productColor === selectedColor;
    const materialMatch = selectedMaterial === 'All' || productMaterial === selectedMaterial;

    return colorMatch && materialMatch;
  });

  const noDataAvailable = selectedMaterial !== 'All' && !filteredProducts.length > 0;

  return (
    <Fragment>
      <div className="container">
        <h2>MYCOOLSHOP.COM</h2>
        <Header />
        <div className="display">
          {/* Loader moved outside of this component */}
          <DisplayList
            selectedColor={selectedColor}
            setSelectedColor={setSelectedColor}
            selectedMaterial={selectedMaterial}
            setSelectedMaterial={setSelectedMaterial}
          />
          <div className="card-container">
            {noDataAvailable ? (
              <span className='no-data'>No data available</span>
            ) : (
              filteredProducts.map((product, index) => (
                <DisplayCard
                  key={index}
                  product={product}
                  color={color}
                  material={material}
                  featured={featured}
                  increaseCartCounter={increaseCartCounter}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default AllProducts;




// here when i make the api promise call in async await in product context i want to add loader until the promise has been resolved and data has arived after that i want the loader to be gone, remove the settime out from all products thats of no use . Do the appropriate changes and give the full code for that same with all changes 


//make the appropoiate changes in filtered product array logic that if one material is chosen and it is true and other i chosen whoich is not available i want to render a no data available text in card contaoiner div 