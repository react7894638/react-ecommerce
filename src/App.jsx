// import React, { useState } from "react";
// import { BrowserRouter as Router, Routes, Route, Link, Navigate } from "react-router-dom";
// import "./App.scss";
// import Header from "./Components/Header";
// import AllProducts from "./Components/AllProducts";
// import FeaturedProducts from "./Components/FeaturedProducts";
// // import Api from "./Api/api"


// function App() {
//   const [cartNumber, setCartNumber] = useState(0)
//   const [productReference, setProductReference] = useState([]);
//   const [productColors, setProductColors] = useState([]);
//   const [productMaterials, setProductMaterials] = useState([]);
//   const [productFeatured, setProductFeatured] = useState([]);


//   const increaseCart = () => {
//     console.log(cartNumber)
//     return setCartNumber(cartNumber + 1);
//   }


//   return (
//     <Router>
//       <div className="container">
//         <h2>MYCOOLSHOP.COM</h2>
//         <Header cartNumber={cartNumber} />
//         <Routes>
//           <Route path="/" element={<Navigate to={"/All-Products"} />} />
//           <Route path="/All-Products" element={<AllProducts
//             increaseCart={increaseCart}
//             productReference={productReference}
//             productColors={productColors}
//             productMaterials={productMaterials}
//             productFeatured={productFeatured} />} />
//           <Route path="/featured-products" element={<FeaturedProducts increaseCart={increaseCart} productFeatured={productFeatured} />} />
//         </Routes>
//         <Api setProductReference={setProductReference}
//           setProductColors={setProductColors}
//           setProductMaterials={setProductMaterials}
//           setProductFeatured={setProductFeatured} />
//       </div>
//     </Router>
//   );
// }

// export default App;
