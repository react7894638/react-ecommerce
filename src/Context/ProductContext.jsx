import React, { createContext, useContext, useEffect, useState } from 'react';
import { productReference, productColor, productMaterial, productFeatured } from '../Api/api';
import Header from '../Components/Header';

const ProductContext = createContext();

export function ProductProvider({ children }) {
    const [allProducts, setAllProducts] = useState([]);
    const [color, setColor] = useState([]);
    const [material, setMaterial] = useState([]);
    const [featured, setFeaturedProducts] = useState([]);
    const [cartCounter, setCartCounter] = useState(0);
    const [isLoading, setIsLoading] = useState(true); // Add loading state

    useEffect(() => {
        const fetchData = async () => {
            try {
                const { data: { products } } = await productReference();
                setAllProducts(products);

                const { data: { colors } } = await productColor();
                setColor(colors);

                const { data: { material } } = await productMaterial();
                setMaterial(material);

                const { data: { featured } } = await productFeatured();
                setFeaturedProducts(featured);

                setIsLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    const increaseCartCounter = () => {
        setCartCounter(cartCounter + 1);
    };

    return (
        <ProductContext.Provider value={{ allProducts, color, material, featured, cartCounter, increaseCartCounter }}>
            {isLoading ? (
                <div className='loading'>
                    <div className="loader">
                    </div>
                </div>
            ) : (
                children
            )}
        </ProductContext.Provider>
    );
}

export function useProductContext() {
    const context = useContext(ProductContext);

    const modifiedProducts = context.allProducts.map(product => {
        if (product.colorId === 1) {
            product.colorId += 1;
        }
        if (product.materialId === 1) {
            product.materialId += 1;
        }
        return product;
    });

    return {
        ...context,
        allProducts: modifiedProducts,
    };
}
