import AllProducts from "../Components/AllProducts";
import FeaturedProducts from "../Components/FeaturedProducts";
import {
    Navigate,
    createBrowserRouter,
} from "react-router-dom";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Navigate to="/all-products" />
    },
    {
        path: "/all-products",
        element: <AllProducts />
    },
    {
        path: "/featured-products",
        element: <FeaturedProducts />
    }
]);

export default router



