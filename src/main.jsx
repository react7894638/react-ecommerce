import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'
import router from './Routes/Routes'
import { ProductProvider } from './Context/ProductContext'

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <ProductProvider>
            <RouterProvider router={router} />
        </ProductProvider>
    </React.StrictMode>

)
